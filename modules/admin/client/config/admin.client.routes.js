'use strict';

// Setting up route
angular.module('admin').config(['$stateProvider',
	function($stateProvider) {
		// Admin state routing
		$stateProvider.
		state('admin', {
			abstract: true,
			url: '/admin',
			template: '<ui-view/>'
		}).state('admin.dashboard', {
			url: '',
			templateUrl: 'modules/admin/views/dashboard.client.view.html'
		}).state('admin.page', {
			url: '/page',
			templateUrl: 'modules/admin/views/add-page.client.view.html'
		}).state('admin.editpage', {
			url: '/page/:pageId',
			templateUrl: 'modules/admin/views/edit-page.client.view.html'
		}).state('admin.pages', {
			url: '/pages',
			templateUrl: 'modules/admin/views/pages.client.view.html'
		}).state('admin.sales', {
			url: '/sales',
			templateUrl: 'modules/admin/views/sales.client.view.html'
		}).state('admin.sale', {
			url: '/sale',
			templateUrl: 'modules/admin/views/add-sales.client.view.html'
		}).state('admin.editSalesPartners', {
			url: '/sales/:salesId',
			templateUrl: 'modules/admin/views/edit-sales.client.view.html'
		}).state('admin.salesdash', {
			url: '/salesdash',
			templateUrl: 'modules/admin/views/salesdash.client.view.html'
		}).state('admin.organization', {
			url: '/organization',
			templateUrl: 'modules/admin/views/add-organization.client.view.html'
		}).state('admin.editorganization', {
			url: '/organization/:organizationsId',
			templateUrl: 'modules/admin/views/edit-organization.client.view.html'
		}).state('admin.organizations', {
			url: '/organizations',
			templateUrl: 'modules/admin/views/organizations.client.view.html'
		}).state('admin.user', {
			url: '/user',
			templateUrl: 'modules/admin/views/add-user.client.view.html'
		}).state('admin.edituser', {
			url: '/user/:adminUserId',
			templateUrl: 'modules/admin/views/edit-user.client.view.html'
		}).state('admin.users', {
			url: '/users',
			templateUrl: 'modules/admin/views/users.client.view.html'
		}).state('admin.offers', {
			url: '/offers',
			templateUrl: 'modules/admin/views/offers.client.view.html'
		}).state('admin.editoffer', {
			url: '/offer/:offerId',
			templateUrl: 'modules/admin/views/edit-offer.client.view.html'
		});
	}
]);