'use strict';

//SalesPartner service used for communicating with the articles REST endpoints
angular.module('admin').factory('SalesPartner', ['$resource',
	function($resource) {
		return $resource('adminapi/sales/:salesId', {
			salesId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
