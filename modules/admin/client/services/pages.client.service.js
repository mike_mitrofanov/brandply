'use strict';

//Pages service used for communicating with the articles REST endpoints
angular.module('admin').factory('Pages', ['$resource',
	function($resource) {
		return $resource('adminapi/pages/:pageId', {
			pageId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
