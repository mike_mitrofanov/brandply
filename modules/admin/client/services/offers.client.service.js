'use strict';

//Offers service used for communicating with the articles REST endpoints
angular.module('admin').factory('Offers', ['$resource',
	function($resource) {
		return $resource('adminapi/local-offers/:offerId', {
			offerId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
