'use strict';

//Organization service used for communicating with the articles REST endpoints
angular.module('admin').factory('Organization', ['$resource',
	function($resource) {
		return $resource('adminapi/organizations/:organizationsId', {
			organizationsId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
