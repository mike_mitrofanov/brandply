'use strict';

//Users service used for communicating with the articles REST endpoints
angular.module('admin').factory('AdminUsers', ['$resource',
	function($resource) {
		return $resource('adminapi/users/:adminUserId', {
			adminUserId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
