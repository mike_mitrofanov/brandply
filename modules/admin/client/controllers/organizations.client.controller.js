'use strict';

angular.module('admin').controller('AdminOrganizationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Organization',
	function($scope, $stateParams, $location, Authentication, Organization) {

		console.log($stateParams);


		$scope.authentication = Authentication;

		$scope.nameText = 'Admin Organizations';

		$scope.organizations = Organization.query();

		$scope.create = function() {
			var organization = new Organization({
				name: this.name,
				url: this.url,
				content: this.content,
				templateColor: this.templateColor,
				emailSettings: this.emailSettings,
				status: this.status
			});

			organization.$save(function(response) {
				$location.path('admin/organizations');

				$scope.name = '';
				$scope.url = '';
				$scope.content = '';
				$scope.templateColor = '';
				$scope.emailSettings = '';
				$scope.status = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			$scope.organization = Organization.get({
				organizationsId: $stateParams.organizationsId
			});
		};

		$scope.update = function() {
			var organization = $scope.organization;

			organization.$update(function() {
				$location.path('admin/organizations');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
	}
]);