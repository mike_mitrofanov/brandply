'use strict';

angular.module('admin').controller('AdminUsersController', ['$scope', '$stateParams', '$location', 'Authentication', 'AdminUsers',
	function($scope, $stateParams, $location, Authentication, AdminUsers) {
		$scope.authentication = Authentication;

		$scope.nameText = 'Admin Users';

		$scope.users = AdminUsers.query();
	
		$scope.create = function() {
			var user = new AdminUsers({
				username: this.username,
				firstName: this.firstname,
				lastName: this.lastname,
				email: this.email,
				password: this.password
			});

			user.$save(function(response) {
				$location.path('admin/users');

				$scope.username = '';
				$scope.firstname = '';
				$scope.lastname = '';
				$scope.email = '';
				$scope.password = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			
			$scope.user = AdminUsers.get({
				adminUserId: $stateParams.adminUserId
			});			

		};

		$scope.update = function() {
			var user = $scope.user;
			user.$update(function() {
				$location.path('admin/users');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
	}
]);