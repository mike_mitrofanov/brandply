'use strict';

angular.module('admin').controller('AdminController', ['$scope', '$stateParams', '$location', 'Authentication',
	function($scope, $stateParams, $location, Authentication) {
		$scope.authentication = Authentication;

		$scope.dashboardText = 'Admin dashboard';
	}
]);