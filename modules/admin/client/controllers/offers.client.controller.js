'use strict';

angular.module('admin').controller('AdminOffersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Offers', 
	function($scope, $stateParams, $location, Authentication, Offers) {
		$scope.authentication = Authentication;

		$scope.offers = Offers.query();

		$scope.nameText = 'Offers page';

		$scope.create = function() {
			var offer = new Offers({
				text: this.text,
				instructions: this.instructions,
				terms: this.terms
			});

			offer.$save(function(response) {
				$location.path('admin/offer');

				$scope.text = '';
				$scope.instructions = '';
				$scope.terms = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			$scope.offer = Offers.get({
				offerId: $stateParams.offerId
			});
		};

		$scope.update = function() {
			var offer = $scope.offer;

			offer.$update(function() {
				$location.path('admin/offers');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

	}
]);