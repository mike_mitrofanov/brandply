'use strict';

angular.module('admin').controller('AdminSalesDashController', ['$scope', '$stateParams', '$location', 'Authentication',
	function($scope, $stateParams, $location, Authentication) {
		$scope.authentication = Authentication;

		$scope.nameText = 'Admin Sales dashboard';
	}
]);