'use strict';

angular.module('admin').controller('AdminPagesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Pages', 
	function($scope, $stateParams, $location, Authentication, Pages) {
		$scope.authentication = Authentication;

		$scope.pages = Pages.query();

		$scope.nameText = 'Admin pages';

		$scope.create = function() {
			var page = new Pages({
				title: this.title,
				content: this.content,
				status: this.status
			});

			page.$save(function(response) {
				$location.path('admin/pages');

				$scope.title = '';
				$scope.content = '';
				$scope.status = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			$scope.page = Pages.get({
				pageId: $stateParams.pageId
			});
		};

		$scope.update = function() {
			var page = $scope.page;

			page.$update(function() {
				$location.path('admin/pages');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

	}
]);