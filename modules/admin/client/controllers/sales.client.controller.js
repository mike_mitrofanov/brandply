'use strict';

angular.module('admin').controller('AdminSalesController', ['$scope', '$stateParams', '$location', 'Authentication', 'SalesPartner',
	function($scope, $stateParams, $location, Authentication, SalesPartner) {
		$scope.authentication = Authentication;

		$scope.nameText = 'Admin SalesPartner';

		$scope.salesPartners = SalesPartner.query();

		$scope.create = function() {
			var salesPartner = new SalesPartner({
				name: this.name
			});

			salesPartner.$save(function(response) {
				$location.path('admin/sales');

				$scope.name = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			$scope.salesPartner = SalesPartner.get({
				salesId: $stateParams.salesId
			});
		};

		$scope.update = function() {
			var salesPartner = $scope.salesPartner;

			salesPartner.$update(function() {
				$location.path('admin/sales');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

	}
]);