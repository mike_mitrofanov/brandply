'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Organization Code Schema
 */
var OrganizationCodeSchema = new Schema({
	organization: {
		type: Schema.ObjectId,
		ref: 'Organization'
	},
	code: {
		type: Schema.ObjectId,
		ref: 'Code'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

var OrganizationCode = mongoose.model('OrganizationCode', OrganizationCodeSchema);