'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Usage Offers Schema
 */
var UsageOffersSchema = new Schema({
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	offer: {
		type: String,
		default: '',
		trim: true
	},
	expired: {
		type: Boolean, 
		default: true
	},
	expiredDate: {
		type: Date
	}
});

var UsageOffers = mongoose.model('UsageOffers', UsageOffersSchema);