'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Organization Admin Schema
 */
var OrganizationAdminSchema = new Schema({
	organization: {
		type: Schema.ObjectId,
		ref: 'Organization'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

var OrganizationAdmin = mongoose.model('OrganizationAdmin', OrganizationAdminSchema);