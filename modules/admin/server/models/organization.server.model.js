'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

/**
 * Organization Schema
 */
var OrganizationSchema = new Schema({
	name: {
		type: String,
		default: '',
		trim: true,
		required: 'Name cannot be blank'
	},
	url: {
		type: String,
		default: '',
		trim: true,
		required: 'URL cannot be blank'
	},
	content: {
		type: String,
		default: '',
		trim: true
	},
	logo: {
		type: String,
		default: '',
		trim: true
	},
	appIcon: {
		type: String,
		default: '',
		trim: true	
	},
	templateColor: {
		type: String,
		default: '',
		trim: true
	},
	theme: {
		type: Schema.ObjectId,
		ref: 'Theme'
	},
	emailSettings: {
		type: String, 
		enum: ['Monthly', 'Weekly', 'EveryDay']
	},
	store: {
		type: String,
		default: '',
		trim: true
	},
	locations: {
		type: String,
		default: '',
		trim: true
	},
	offer: {
		type: String,
		default: '',
		trim: true
	},
	coordinates:{
        type: [Number]
    },
	status: {
		type: String, 
		enum: ['Active', 'Blocked']
	},
	created: {
		type: Date,
		default: Date.now
	}
});

OrganizationSchema.index({ coordinates: '2dsphere' });

OrganizationSchema.plugin(autoIncrement.plugin, {
    model: 'Organization',
    field: 'organizationId',
    startAt: 1,
    incrementBy: 1
});

var Organization = mongoose.model('Organization', OrganizationSchema);

/*
var OrganizationFirst = new Organization({title: 'My First Organization'});

OrganizationFirst.save(function(err, OrganizationFirst) {
	console.log(OrganizationFirst.organizationId);
});

var OrganizationTwo = new Organization({title: 'My Two Organization'});

OrganizationTwo.save(function(err, OrganizationTwo) {
	console.log(OrganizationTwo.organizationId);
});
*/