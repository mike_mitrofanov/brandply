'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	URLSlugs = require('mongoose-url-slugs');

/**
 * Page Schema
 */
var PageSchema = new Schema({
	title: {
		type: String,
		default: '',
		trim: true,
		required: 'Title cannot be blank'
	},
	content: {
		type: String,
		default: '',
		trim: true
	},
	status: {
		type: String, 
		enum: ['Active', 'Draft']
	},
	created: {
		type: Date,
		default: Date.now
	}
});

// Save slugs to 'slug' field. 
PageSchema.plugin(new URLSlugs('title', {field: 'slug'}));

var Page = mongoose.model('Page', PageSchema);
/*
var pageFirst = new Page({title: 'My First Page trytryrty', content: 'DosdfvSDV SDV SDVdsV e ery ey sdfvb Dv DV v awvwv wv sev e', status: 'Draft'});

pageFirst.save(function(err, pageFirst) {
	console.log(pageFirst.slug);
});*/