'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Theme Schema
 */
var ThemeSchema = new Schema({
	name: {
		type: String,
		default: '',
		trim: true,
		required: 'Name cannot be blank'
	},
	status: {
		type: String,
		enum: ['Active', 'Draft']
	},
	created: {
		type: Date,
		default: Date.now
	}
});


var Theme = mongoose.model('Theme', ThemeSchema);
/*
var themeFirst = new Theme({name: 'My First Theme', status: 'Active'});

themeFirst.save(function(err, themeFirst) {
	console.log(themeFirst.name);
});
*/