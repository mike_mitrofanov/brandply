'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Sales Partner Schema
 */
var SalesPartnerSchema = new Schema({
	/*user: {
		type: Schema.ObjectId,
		ref: 'User'
	}*/
	name: {
		type: String,
		default: '',
		trim: true,
		required: 'Name cannot be blank'
	},
	startData: {
		type: Date,
		default: Date.now
	}
});


var SalesPartner = mongoose.model('SalesPartner', SalesPartnerSchema);
/*
var SalesPartnerFirst = new SalesPartner();

SalesPartnerFirst.save(function(err, SalesPartnerFirst) {
	console.log(SalesPartnerFirst.startData);
});
*/