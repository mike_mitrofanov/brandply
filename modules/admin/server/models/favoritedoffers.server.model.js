'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Favorited Offers Schema
 */
var FavoritedOffersSchema = new Schema({
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	offer: {
		type: String,
		default: '',
		trim: true
	},
	expired: {
		type: Boolean, 
		default: true
	},
	expiredDate: {
		type: Date
	}
});

var FavoritedOffers = mongoose.model('FavoritedOffers', FavoritedOffersSchema);