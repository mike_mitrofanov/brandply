'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Code Schema
 */
var CodeSchema = new Schema({
	code: {
		type: String,
		default: '',
		trim: true,
		required: 'Code cannot be blank'
	},
	hashedCode: {
		type: String,
		default: '',
		trim: true,
	},
	status: {
		type: String,
		enum: ['Active', 'Draft']
	},
	created: {
		type: Date,
		default: Date.now
	}
});


var Code = mongoose.model('Code', CodeSchema);
/*
var codeFirst = new Code({name: 'My First Code', status: 'Active'});

codeFirst.save(function(err, codeFirst) {
	console.log(codeFirst.code);
});
*/