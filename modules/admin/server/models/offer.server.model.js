'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Offer Schema
 */
var OfferSchema = new Schema({
	text: {
		type: String,
		default: '',
		trim: true,
		required: 'Text cannot be blank'
	},
	instructions: {
		type: String,
		default: '',
		trim: true
	},
	terms: {
		type: String,
		default: '',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

var Offer = mongoose.model('Offer', OfferSchema);
/*
var offerFirst = new Offer({text: 'My First Page trytryrty', instructions: 'DosdfvSDV', terms: 'Draftdf'});

offerFirst.save(function(err, offerFirst) {
	console.log(offerFirst.text);
});
*/