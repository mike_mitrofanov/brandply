'use strict';

/**
 * Module dependencies.
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Offers Permissions
 */
exports.invokeRolesPolicies = function() {
	acl.allow([{
		roles: ['admin'],
		allows: [{
			resources: '/admin',
			permissions: '*'
		},{
			resources: '/api/autocomplete/:autocompleteParams',
			permissions: '*'
		},{
			resources: '/api/categories/:categoriesParams',
			permissions: '*'
		},{
			resources: '/api/locations/:locationsParams',
			permissions: '*'
		},{
			resources: '/api/offers/:offersParams',
			permissions: '*'
		},{
			resources: '/api/stores/:storesParams',
			permissions: '*'
		},{
			resources: '/adminapi/pages',
			permissions: '*'
		},{
			resources: '/adminapi/pages/:pageId',
			permissions: '*'
		}]
	}, {
		roles: ['user'],
		allows: [{
			resources: '/api/offers/:offersParams',
			permissions: ['get']
		},{ // ONLY TEST 
			resources: '/adminapi/pages',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/pages/:pageId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/users',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/users/:adminUserId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/sales',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/sales/:salesId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/organizations',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/organizations/:organizationsId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/local-offers',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/local-offers/:offerId',
			permissions: '*'
		}]
	}, {
		roles: ['guest'],
		allows: [{
			resources: '/api/autocomplete/:autocompleteParams',
			permissions: ['get']
		},{
			resources: '/api/categories/:categoriesParams',
			permissions: ['get']
		},{
			resources: '/api/locations/:locationsParams',
			permissions: ['get']
		},{
			resources: '/api/offers/:offersParams',
			permissions: ['get']
		},{
			resources: '/api/stores/:storesParams',
			permissions: ['get']
		},{ // ONLY TEST 
			resources: '/adminapi/pages',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/pages/:pageId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/users',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/users/:adminUserId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/sales',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/sales/:salesId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/organizations',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/organizations/:organizationsId',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/local-offers',
			permissions: '*'
		},{ // ONLY TEST 
			resources: '/adminapi/local-offers/:offerId',
			permissions: '*'
		}]
	}]);
};

/**
 * Check If Offers Policy Allows
 */
exports.isAllowed = function(req, res, next) {
	var roles = (req.user) ? req.user.roles : ['guest'];

	// Check for user roles
	acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function(err, isAllowed) {
		if (err) {
			// An authorization error occurred.
			return res.status(500).send('Unexpected authorization error');
		} else {
			if (isAllowed) {
				// Access granted! Invoke next middleware
				return next();
			} else {
				return res.status(403).json({
					message: 'User is not authorized :)',
					isAllowed: isAllowed
				});
			}
		}
	});
};
