'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
	mongoose = require('mongoose'),
	Offer = mongoose.model('Offer'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of offers
 */
exports.list = function(req, res) {
	Offer.find().exec(function(err, offers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(offers);
		}
	});
};

/**
 * Show the current offer
 */
exports.read = function(req, res) {
	res.json(req.offer);
};

/**
 * Create a offer
 */
exports.create = function(req, res) {
	var offer = new Offer(req.body);

	offer.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(offer);
		}
	});
};

/**
 * Update a offer
 */
exports.update = function(req, res) {
	var offer = req.offer;

	offer.text = req.body.text;
	offer.instructions = req.body.instructions;
	offer.terms = req.body.terms;

	offer.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(offer);
		}
	});
};

/**
 * Offer middleware
 */
exports.offerId = function(req, res, next, id) {
	Offer.findById(id).exec(function(err, offer) {
		if (err) return next(err);
		if (!offer) return next(new Error('Failed to load offer ' + id));
		req.offer = offer;
		next();
	});
};