'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
	config = require(path.resolve('./config/config')), // add Global config
	http = require('http'),
	https = require('https'),
	mongoose = require('mongoose'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Offers
 */
exports.list = function(req, res, next, autocompleteParams) {

	var options = {
		host: config.accessdevelopment.host,
		port: config.accessdevelopment.port,
		path: '/'+config.accessdevelopment.version+'/autocomplete?access_token='+config.accessdevelopment.access_token+'&member_key='+config.accessdevelopment.member_key
	};

	if(typeof( autocompleteParams ) === 'string'){
		options.path += ('&' + autocompleteParams);
	}

	https.get(options, function(resp){

		var body = '';
		
	  	resp.on('data', function(chunk){
	  		body += chunk;
	  	});

	  	resp.on('end', function() {
	  		res.json(JSON.parse(body));
	    });	  	

	}).on('error', function(e){
	  	console.log('Got error: ' + e.message);
	});
};