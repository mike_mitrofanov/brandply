'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
	mongoose = require('mongoose'),
	Organization = mongoose.model('Organization'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Pages
 */
exports.list = function(req, res) {
	Organization.find().exec(function(err, organizations) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(organizations);
		}
	});
};

/**
 * Show the current organization
 */
exports.read = function(req, res) {
	res.json(req.organization);
};

/**
 * Create a organization
 */
exports.create = function(req, res) {
	var organization = new Organization(req.body);

	organization.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(organization);
		}
	});
};

/**
 * Update a organization
 */
exports.update = function(req, res) {
	var organization = req.organization;

	organization.name = req.body.name;
	organization.url = req.body.url;
	organization.content = req.body.content;
	organization.templateColor = req.body.templateColor;
	organization.emailSettings = req.body.emailSettings;
	organization.status = req.body.status;

	organization.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(organization);
		}
	});
};

/**
 * Organization middleware
 */
exports.organizationsId = function(req, res, next, id) {
	Organization.findById(id).exec(function(err, organization) {
		if (err) return next(err);
		if (!organization) return next(new Error('Failed to load organization ' + id));
		req.organization = organization;
		next();
	});
};