'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
	mongoose = require('mongoose'),
	SalesPartner = mongoose.model('SalesPartner'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Pages
 */
exports.list = function(req, res) {
	SalesPartner.find().exec(function(err, salesPartners) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(salesPartners);
		}
	});
};

/**
 * Show the current salesPartner
 */
exports.read = function(req, res) {
	res.json(req.salesPartner);
};

/**
 * Create a salesPartner
 */
exports.create = function(req, res) {
	var salesPartner = new SalesPartner(req.body);

	salesPartner.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(salesPartner);
		}
	});
};

/**
 * Update a sales
 */
exports.update = function(req, res) {
	var salesPartner = req.salesPartner;

	salesPartner.name = req.body.name;

	salesPartner.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(salesPartner);
		}
	});
};

/**
 * SalesPartner middleware
 */
exports.salesId = function(req, res, next, id) {
	SalesPartner.findById(id).exec(function(err, salesPartner) {
		if (err) return next(err);
		if (!salesPartner) return next(new Error('Failed to load salesPartner ' + id));
		req.salesPartner = salesPartner;
		next();
	});
};