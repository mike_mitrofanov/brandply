'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
	mongoose = require('mongoose'),
	Page = mongoose.model('Page'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Pages
 */
exports.list = function(req, res) {
	Page.find().exec(function(err, pages) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(pages);
		}
	});
};

/**
 * Show the current page
 */
exports.read = function(req, res) {
	res.json(req.page);
};

/**
 * Create a page
 */
exports.create = function(req, res) {
	var page = new Page(req.body);

	page.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(page);
		}
	});
};

/**
 * Update a page
 */
exports.update = function(req, res) {
	var page = req.page;

	page.title = req.body.title;
	page.content = req.body.content;
	page.status = req.body.status;

	page.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(page);
		}
	});
};

/**
 * Page middleware
 */
exports.pageId = function(req, res, next, id) {
	Page.findById(id).exec(function(err, page) {
		if (err) return next(err);
		if (!page) return next(new Error('Failed to load page ' + id));
		req.page = page;
		next();
	});
};