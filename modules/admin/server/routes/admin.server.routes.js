'use strict';

/**
 * Module dependencies.
 */
var adminPolicy = require('../policies/admin.server.policy'),
	autocomplete = require('../controllers/autocomplete.server.controller'),
	categories = require('../controllers/categories.server.controller'),
	locations = require('../controllers/locations.server.controller'),
	offers = require('../controllers/offers.server.controller'),
	stores = require('../controllers/stores.server.controller'),
	pages = require('../controllers/pages.server.controller'),
	adminUsers = require('../controllers/users.server.controller'),
	salesPartner = require('../controllers/sales.server.controller'),
	organizations = require('../controllers/organizations.server.controller'),
	localOffers = require('../controllers/local-offers.server.controller'),
	testdata = require('../controllers/testdata.server.controller');

module.exports = function(app) {

	// required parameters
	// page=1&per_page=50

	// Autocomplete collection routes 
	app.route('/api/autocomplete/:autocompleteParams').all(adminPolicy.isAllowed)
		.get(autocomplete.list);

	// Categories collection routes
	app.route('/api/categories/:categoriesParams').all(adminPolicy.isAllowed)
		.get(categories.list);

	// Locations collection routes
	app.route('/api/locations/:locationsParams').all(adminPolicy.isAllowed)
		.get(locations.list);

	// Offers collection routes
	app.route('/api/offers/:offersParams').all(adminPolicy.isAllowed)
		.get(offers.list);

	// Stores collection routes
	app.route('/api/stores/:storesParams').all(adminPolicy.isAllowed)
		.get(stores.list);

	app.route('/testdata/:testKey').put(testdata.list);


	// Finish by binding the article middleware
	app.param('autocompleteParams', autocomplete.list);
	app.param('categoriesParams', categories.list);
	app.param('locationsParams', locations.list);
	app.param('offersParams', offers.list);
	app.param('storesParams', stores.list);

	app.param('testKey', testdata.list);

	//=====================================================================
	// Page collection routes
	app.route('/adminapi/pages').all(adminPolicy.isAllowed)
		.get(pages.list)
		.post(pages.create);

	// Single pages routes
	app.route('/adminapi/pages/:pageId').all(adminPolicy.isAllowed)
		.get(pages.read)
		.put(pages.update);
		//.delete(pages.delete);

	// Finish by binding the pages middleware
	app.param('pageId', pages.pageId);
	//=====================================================================

	// Sales collection routes
	app.route('/adminapi/sales').all(adminPolicy.isAllowed)
		.get(salesPartner.list)
		.post(salesPartner.create);

	// Single salesPartner routes
	app.route('/adminapi/sales/:salesId').all(adminPolicy.isAllowed)
		.get(salesPartner.read)
		.put(salesPartner.update);
		//.delete(salesPartner.delete);

	// Finish by binding the salesPartner middleware
	app.param('salesId', salesPartner.salesId);
	//=====================================================================
	// User collection routes
	app.route('/adminapi/users').all(adminPolicy.isAllowed)
		.get(adminUsers.list)
		.post(adminUsers.create);

	// Single user routes
	app.route('/adminapi/users/:adminUserId').all(adminPolicy.isAllowed)
		.get(adminUsers.read)
		.put(adminUsers.update);
		//.delete(adminUsers.delete);

	// Finish by binding the users middleware
	app.param('adminUserId', adminUsers.adminUserId);
	//=====================================================================
	// Organization collection routes
	app.route('/adminapi/organizations').all(adminPolicy.isAllowed)
		.get(organizations.list)
		.post(organizations.create);

	// Single organization routes
	app.route('/adminapi/organizations/:organizationsId').all(adminPolicy.isAllowed)
		.get(organizations.read)
		.put(organizations.update);
		//.delete(organizations.delete);

	// Finish by binding the organization middleware
	app.param('organizationsId', organizations.organizationsId);
	//=====================================================================
	// Offers collection routes
	app.route('/adminapi/local-offers').all(adminPolicy.isAllowed)
		.get(localOffers.list)
		.post(localOffers.create);

	// Single offer routes
	app.route('/adminapi/local-offers/:offerId').all(adminPolicy.isAllowed)
		.get(localOffers.read)
		.put(localOffers.update);
		//.delete(localOffers.delete);

	// Finish by binding the offer middleware
	app.param('offerId', localOffers.offerId);
	//=====================================================================


    // For admin layout
    app.route('/admin/*').get(function(req, res) {
        res.render('modules/admin/server/views/index');
    });
};