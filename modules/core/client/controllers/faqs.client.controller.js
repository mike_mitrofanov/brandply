'use strict';

angular.module('core').controller('FaqsController', ['$scope', 'Authentication',
    function($scope, Authentication) {
        // This provides Authentication context.
        $scope.authentication = Authentication;
        $scope.search = '';

        //get questions from back-end
        $scope.faqs = [
            {
                title: 'What is Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'Who is Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'How Do I Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'When do I Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'What is Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'Who is Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'How Do I Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            },
            {
                title: 'When do I Brandply?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a pretium nisi, at bibendum velit. Aenean nec orci diam. Morbi vitae lectus pharetra purus porta aliquet. Donec a sem orci. Vivamus rhoncus nibh sapien, at iaculis neque semper vel. Nullam vehicula mauris nulla, a rutrum ligula convallis vitae. Integer porta turpis quis sapien efficitur porttitor. Ut molestie et turpis sed mattis.'
            }
        ];
    }
]);