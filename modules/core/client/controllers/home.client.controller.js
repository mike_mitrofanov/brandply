'use strict';

angular.module('core').controller('HomeController', ['$scope', '$window', 'Authentication', '$http', 'Core',
    function($scope, $window, Authentication, $http, Core) {
        // This provides Authentication context.

        //console.log('wd', Core.get({offerParams: 'page=1&per_page=2'}));

        $scope.postalKey = '';
        var n = 1;
        var lat = '';
        var lon = '';
        var data = {};
        var scrollFlag = false;

        window.navigator.geolocation.getCurrentPosition(function(pos) {
            //console.log(pos);
            lat = pos.coords.latitude;
            lon = pos.coords.longitude;
        });

        $scope.authentication = Authentication;


        Core.get({offerParams: 'page=1&per_page=8'}, function(data){
            //console.log('we', data);
            getMilesData(data);
            $scope.allOffers = data;
            scrollFlag = true;
        });


        angular.element($window).bind("scroll", function() {
            if(!scrollFlag){
                return false;
            }
            var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
            var body = document.body, html = document.documentElement;
            var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
            var windowBottom = windowHeight + window.pageYOffset;
            if (windowBottom >= docHeight) {
            var postalString = '';
                if($scope.postalKey !== ''){
                    //console.log($scope.postalKey);
                    postalString = '&postal_code='+$scope.postalKey;
                    //return false;
                }

                angular.element(document.querySelector('.preloader-offers')).css('display', 'block');
                n++;
                //console.log(n);
                //console.log('bottom reached');
                scrollFlag = false;

                Core.get({offerParams: 'api/offers/page='+n+'&per_page=8'+postalString}, function(data){
                    getMilesData(data);

                    if($scope.allOffers.length !== 0){
                        var newOffers = data.offers;
                        var stable = $scope.allOffers.offers;
                        var children = stable.concat(newOffers);
                        //console.log(data.offers);
                        //console.log($scope.allOffers.offers);
                        //console.log(children);
                        $scope.allOffers.offers = children;
                    }

                    else{
                        $scope.allOffers = data;
                    }


                    angular.element(document.querySelector('.preloader-offers')).css('display', 'none');
                    scrollFlag = true;
                });

            }
        });

        $scope.offersSearch = function(postalKey){

            if(postalKey == '' || isNaN(postalKey)){
                $scope.postalKey = '';
                $scope.allOffers = {};
                n = 1;
                scrollFlag = false;
                return false;
            }
            $scope.allOffers = {};
            angular.element(document.querySelector('.preloader-offers')).css('display', 'block');
            $scope.postalKey = postalKey;
            Core.get({offerParams: 'api/offers/page=1&postal_code='+$scope.postalKey+'&per_page=8'}, function(data){
                getMilesData(data);

                $scope.allOffers = data;
                angular.element(document.querySelector('.preloader-offers')).css('display', 'none');
                //console.log($scope.allOffers);
            });
        };

        $scope.onFolderNumberKeyPress = function(event, postalKey){
            if (event.charCode == 13){
                $scope.offersSearch(postalKey);
            }
        };

        function distance(lon1, lat1, lon2, lat2) {
            var R = 6371; // Radius of the earth in km
            var dLat = (lat2-lat1).toRad();  // Javascript functions in radians
            var dLon = (lon2-lon1).toRad();
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
                Math.sin(dLon/2) * Math.sin(dLon/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            //console.log('km', d);
            d = d  * 0.62137;
            return Math.round(d);
        }

        /** Converts numeric degrees to radians */
        if (typeof(Number.prototype.toRad) === "undefined") {
            Number.prototype.toRad = function() {
                return this * Math.PI / 180;
            }
        }

        function getMilesData(data){
            if(lat != '' && lon != ''){
                angular.forEach(data.offers, function(value, key) {
                    //console.log(value.offer_store.physical_location.geolocation);
                    //console.log(distance(lon, lat, value.offer_store.physical_location.geolocation.lon, value.offer_store.physical_location.geolocation.lat));
                    data.offers[key].distance = distance(lon, lat, value.offer_store.physical_location.geolocation.lon, value.offer_store.physical_location.geolocation.lat);
                });
            }

            scrollFlag = true;

        }

    }
]);
