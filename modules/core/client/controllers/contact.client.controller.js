'use strict';

angular.module('core').controller('ContactController', ['$scope', 'Authentication', '$http',
    function($scope, Authentication, $http) {
        // This provides Authentication context.
        $scope.authentication = Authentication;

        $scope.contact = function() {
            $scope.submitted = true;
            if(!$scope.contactForm.$valid) {
                $scope.error = 'Fill all required fields';
                return false;
            }

            $http.post('api/contact', $scope.contact).success(function(response) {
                //TODO code after sending contact email
            }).error(function(response) {
                $scope.error = response.message;
            });
        };
    }
]);
