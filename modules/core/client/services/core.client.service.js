'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Core', ['$resource',
    function($resource) {
        return $resource('/api/offers/:offerParams', {offerParams: '@params'});
    }
]);
