'use strict';

// Setting up route
angular.module('core').config(['$stateProvider',
    function($stateProvider) {
        // Articles state routing
        $stateProvider
            .state('business', {
                url: '/business',
                templateUrl: 'modules/core/views/business.client.view.html'
            })
            .state('consumers', {
                url: '/consumers',
                templateUrl: 'modules/core/views/consumers.client.view.html'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'modules/core/views/page.client.view.html'
            })
            .state('howToUse', {
                url: '/howToUse',
                templateUrl: 'modules/core/views/page.client.view.html'
            })
            .state('faqs', {
                url: '/faqs',
                templateUrl: 'modules/core/views/faqs.client.view.html'
            })
            .state('privacypolicy', {
                url: '/privacypolicy',
                templateUrl: 'modules/core/views/page.client.view.html'
            })
            .state('contact', {
                url: '/contact',
                templateUrl: 'modules/core/views/contact.client.view.html'
            });
    }
]);