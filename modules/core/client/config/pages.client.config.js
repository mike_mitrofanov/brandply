'use strict';

// Configuring the Articles module
angular.module('core').run(['Menus',
    function(Menus) {

        /*top menu*/
        Menus.addMenuItem('topbar', {
            title: 'Business',
            state: 'business'
        });

        Menus.addMenuItem('topbar', {
            title: 'Consumers',
            state: 'consumers'
        });

        Menus.addMenuItem('topbar', {
            title: 'Activate',
            state: 'authentication.signup',
            shouldRender: function(user) {
                if (user === null || typeof user === 'undefined' || user === '') {
                    return true;
                } else {
                    return false;
                }
            }
        });

        /*bottom menu*/
        Menus.addMenuItem('bottombar', {
            title: 'About',
            state: 'about'
        });

        Menus.addMenuItem('bottombar', {
            title: 'How To Use',
            state: 'howToUse'
        });

        Menus.addMenuItem('bottombar', {
            title: 'FAQs',
            state: 'faqs'
        });

        Menus.addMenuItem('bottombar', {
            title: 'Privacy Policy',
            state: 'privacypolicy'
        });

        Menus.addMenuItem('bottombar', {
            title: 'Contact',
            state: 'contact'
        });

        Menus.addMenuItem('bottombar', {
            title: 'Business',
            state: 'business'
        });

        Menus.addMenuItem('bottombar', {
            title: 'Consumers',
            state: 'consumers'
        });
    }
]);
