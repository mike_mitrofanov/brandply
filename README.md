# brandply1-1
Brandply web application v 1.1

# API

Currently the work with following entities is implemented in API:
- Offers
- Categories
- Stores
- Locations
- Autocomplete

Connection settings are located here:

- production environment
/brandply1-1/config/env/production.js

- development environment
/brandply1-1/config/env/development.js

An example of filling in setting of development environment

accessdevelopment: {
        host: 'offer-demo.adcrws.com',
        port: 443,
        access_token: 'a87c0c4555f02fd671c4deab63e7bab8bac24cfb9d9e0edbd1e865308eb7a126',
        member_key: 10613,
        version: 'v1'
    }

// *******************************************

Getting all data is implemented via GET queries

Upon calling API there are 3 mandatory params
- "page"
- "per_page"

An example of call to get offers list
/api/offers/page=1&per_page=10

Every reply except Autocomplete returns general entity info

"info": {
        "total_results": 206577,
        "current_page": 1,
        "total_pages": 20658,
        "results_per_page": 10
    }

// *******************************************

Calls list for entities

1) Offers
/api/offers/page=1&per_page=10

Also all params from API accessdevelopment are available

https://docs-demo.accessdevelopment.com/api/offers/search.html#parameters

2) Categories 
/api/categories/page=1&per_page=10

Also all params from API accessdevelopment are available

https://docs-demo.accessdevelopment.com/api/categories/search.html#parameters

3) Stores
/api/stores/page=1&per_page=10

Also all params from API accessdevelopment are available

https://docs-demo.accessdevelopment.com/api/stores/search.html#parameters

4) Locations
/api/locations/page=1&per_page=10

Also all params from API accessdevelopment are available

https://docs-demo.accessdevelopment.com/api/locations/search.html#parameters

5) Autocomplete
/api/autocomplete/page=1&per_page=10

Also all params from API accessdevelopment are available

https://docs-demo.accessdevelopment.com/api/autocomplete/search.html#parameters

*******************************************************************************
*******************************************************************************
*******************************************************************************

Db structure. Version 1.0

List of entities:

- User (users storing)
- Page (Static pages storing)

- Theme (List of themes for extended Organization settings)
- Code

- Organization
- OrganizationAdmin - ( List of users who are admins for organizations )
- OrganizationCode (storing code relations of users and organizations)

- SalesPartner

- FavoritedOffers ( List of favorites for user )
- UsageOffers (user's used offers)

=========================================================

// The list of additional fields for entity User

roles: enum: ['user', 'admin', 'organization_admin', 'sales_partner']
userId: autoIncrement
profileImageURL: String

=========================================================

// The list of additional fields for entity Page

slug: URLSlugs
status: enum: ['Active', 'Draft']

=========================================================

// The list of additional fields for entity Theme

status: enum: ['Active', 'Draft']

=========================================================

// The list of additional fields for entity Code

status: enum: ['Active', 'Draft']

=========================================================

// The list of additional fields for entity Organization

=========================================================

// The list of additional fields for entity OrganizationAdmin

organization: ref: 'Organization'
user: ref: 'User'

=========================================================

// The list of additional fields for entity OrganizationCode

organization: ref: 'Organization'
code: ref: 'Code'
user: ref: 'User'

=========================================================

// The list of additional fields for entity SalesPartner

user: ref: 'User'

=========================================================

// The list of additional fields for entity FavoritedOffers

user: ref: 'User'
offer: type: String
expired: type: Boolean
expiredDate: type: Date 

=========================================================

// The list of additional fields for entity UsageOffers

user: ref: 'User'
offer: type: String
expired: type: Boolean
expiredDate: type: Date 